//
//  WVPlayer.h

#import <Cordova/CDV.h>

@interface WVPlayer : CDVPlugin

- (void) getURL:(CDVInvokedUrlCommand*)command;

@end
