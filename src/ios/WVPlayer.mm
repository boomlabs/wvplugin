//
//  WVPlayer.mm

#import "WVPlayer.h"
#import "WViPhoneAPI.h"


static NSString * const kEMMURL = @"http://widevine.ezdrm.com/widevine/cypherpc/cgi-bin/GetEMMs-ezdrm.cgi";
static NSString * const kPortal = @"movidone";
static NSString * const kOptData = @"ezAFD0D2;idUser=anonymous;idContent=4755";
static NSString * const kWVMUrl = @"http://voddrm.tellypod.com/MadamSecretary/s1/e5/MadamSecretary-s1-e5-MY_432_phone.wvm";

extern "C" {
    void LogEvent(WViOsApiEvent ev)
    {
        NSLog(@"WViOsApiEvent [%@]\n", NSStringFromWViOsApiEvent(ev));
    }
    
    WViOsApiStatus WVCallback( WViOsApiEvent event, NSDictionary *attributes )
    {
        NSLog( @"callback %d %@ %@\n", event, NSStringFromWViOsApiEvent( event ), attributes );
        
        switch ( event ) {
            case WViOsApiEvent_SetCurrentBitrate:
                NSLog(@"WViOsApiEvent_SetCurrentBitrate");
                break;
            case WViOsApiEvent_Bitrates:
                NSLog(@"WViOsApiEvent_Bitrates");
                break;
            case WViOsApiEvent_ChapterTitle:
                NSLog(@"WViOsApiEvent_ChapterTitle");
                break;
            case WViOsApiEvent_ChapterImage:
                NSLog(@"WViOsApiEvent_ChapterImage");
                break;
            case WViOsApiEvent_ChapterSetup:
                NSLog( @"WViOsApiEvent_ChapterSetup\n" );
                break;
            case WViOsApiEvent_QueryStatus:
                NSLog(@"WViOsApiEvent_QueryStatus EVENT");
                if([attributes objectForKey:WVAssetIDKey]){
                    NSLog(@"EVENT %@", [NSString stringWithFormat: @"[ID: %@]   [Distribution time: %@ secs] [Purchase time: %@ secs]   [EMM Time: %@ secs]", [attributes objectForKey:WVAssetIDKey], [attributes objectForKey:WVDistributionTimeRemainingKey],  [attributes objectForKey:WVPurchaseTimeRemainingKey], [attributes objectForKey:WVEMMTimeRemainingKey]]);
                }
                break;
            case WViOsApiEvent_Stopped:
                NSLog(@"WViOsApiEvent_Stopped");
                break;
            case WViOsApiEvent_StoppingOnError:
                NSLog(@"WViOsApiEvent_StoppingOnError");
                break;
            case WViOsApiEvent_EMMFailed:
                NSLog(@"WViOsApiEvent_EMMFailed");
                
                break;
            case WViOsApiEvent_EMMReceived:
                NSLog(@"WViOsApiEvent_EMMReceived");
                break;
        }
        
        return WViOsApiStatus_OK;
    }
};


@implementation WVPlayer

- (void)getURL:(CDVInvokedUrlCommand*)command
{
    
    NSString *callbackId = [command callbackId];
    NSString *optUrl = [[command arguments] objectAtIndex:0];
    NSString *optData = [[command arguments] objectAtIndex:1];
    
    optUrl = [optUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    WV_Stop();
    WViOsApiStatus wvApiStatus;
    WV_Initialize(WVCallback, @{
                                WVDRMServerKey   : kEMMURL,
                                WVPortalKey      : kPortal,
                                WVDeviceIdKey    : kPortal,
                                WVCAUserDataKey  : optData
                                });
    NSLog(@"Initializing video player");
   
    WV_UnregisterAsset(optUrl);
    
    NSMutableString *responseUrl = [NSMutableString string];
    NSLog(@"URL to pass to MP [%@]", responseUrl);
    
    wvApiStatus = WV_Play( optUrl, responseUrl, 0 );
    if (wvApiStatus != WViOsApiStatus_OK) {
        NSLog(@"WV_Play: ERROR [%d]", wvApiStatus);
        responseUrl = [NSMutableString stringWithFormat:@"ERROR_%d", wvApiStatus];
    } else {
        NSLog(@"WV_Play: OK");
    }
    
    
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsString:responseUrl];
    
    [self success:result callbackId:callbackId];
}

@end