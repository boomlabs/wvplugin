/*global cordova, module*/

module.exports = {
    getURL: function (stream,optData,successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "WVPlayer", "getURL", [stream,optData]);
    }
};
